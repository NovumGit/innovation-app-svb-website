<?php

return [
    'config_dir'  => 'novum.svb',
    'namespace'   => 'NovumSvb',
    'protocol'    => isset($_SERVER['IS_DEVEL']) ? 'http' : 'https',
    'live_domain' => 'svb.demo.novum.nu',
    'dev_domain'  => 'svb.demo.novum.nuidev.nl',
    'test_domain' => 'svb.test.demo.novum.nu',
];
